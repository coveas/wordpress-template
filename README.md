# WordPress Template

## Installation

```
composer create-project cove/wordpress-template project-name
```

## Updating

```
sh -c "$(curl -fsSL https://gitlab.com/coveas/wordpress-template/-/raw/master/updater.sh)"
```
