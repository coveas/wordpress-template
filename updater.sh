#!/bin/bash

# Update files
git clone https://gitlab.com/coveas/wordpress-template.git
cp wordpress-template/public/wp-content/mu-plugins/*.php public/wp-content/mu-plugins/
cp wordpress-template/public/*.php public/
cp wordpress-template/.editorconfig ./
cp wordpress-template/.sublime-settings ./
cp wordpress-template/*.php ./
cp wordpress-template/*.xml ./
cp wordpress-template/wp-cli.yml ./
cp wordpress-template/mysql-params.php ./
chmod 700 generate-salt.php
chmod 700 mysql-params.php

rm -rf wordpress-template

# Composer packages
composer remove drivdigital/wordpress-config vlucas/phpdotenv composer/installers
composer require cove/wordpress-config
