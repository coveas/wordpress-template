<?php
/**
 * This file is part of WordPress Template.
 *
 * @package cove/wordpress-template
 */

namespace Cove;

require_once __DIR__ . '/autoload.php';
require_once ABSPATH . 'wp-settings.php';
