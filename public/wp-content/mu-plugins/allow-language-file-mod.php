<?php
/**
 * Plugin Name: Allow language file mod
 * Plugin URI: https://gitlab.com/coveas/wordpress-template
 * Description:  Allow language packs to be downloaded
 * Author: Eivin Landa
 * Author URI: https://cove.no
 * License: GPL-3.0 or later
 *
 * @version 1.0.12
 * @package cove/wordpress-template
 */

add_filter(
	'file_mod_allowed',
	function ( $allowed, $context ) {
		if ( 'download_language_pack' === $context ) {
			return true;
		}
		return $allowed;
	},
	10,
	2
);
