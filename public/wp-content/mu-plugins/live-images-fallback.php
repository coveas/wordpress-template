<?php
/**
 * Plugin Name: Live image fallback
 * Plugin URI: https://gitlab.com/coveas/wordpress-template
 * Description: Uses images from the live site if the files are not found locally.
 * Author: Eivin Landa
 * Author URI: https://cove.no
 * License: GPL-3.0 or later
 *
 * @version 1.0.9
 * @package cove/wordpress-template
 */

namespace Cove;

if ( env( 'LIVE_URL' ) && env( 'APP_ENV' ) && 'production' !== env( 'APP_ENV' ) ) {
	add_filter(
		'wp_calculate_image_srcset',
		function( $image_srcset ) use ( $public_path ) {
			$live_url = env( 'LIVE_URL' );
			if ( ! preg_match( '/^https?:\/\//', $live_url ) ) {
				$live_url = "https://$live_url";
			}
			foreach ( $image_srcset as &$image_src ) {
				$path = preg_replace(
					'/^' . str_replace( '/', '\/', preg_quote( get_home_url() ) ) . '/',
					'',
					$image_src['url']
				);
				if ( ! file_exists( $public_path . $path ) ) {
					$image_src['url'] = preg_replace( '/\/$/', '', $live_url ) . $path;
				}
			}
			return $image_srcset;
		}
	);
	add_filter(
		'wp_get_attachment_image_src',
		function( $image_src ) use ( $public_path ) {
			if ( ! $image_src ) {
				return $image_src;
			}
			$live_url = env( 'LIVE_URL' );
			if ( ! preg_match( '/^https?:\/\//', $live_url ) ) {
				$live_url = "https://$live_url";
			}
			$path = preg_replace(
				'/^' . str_replace( '/', '\/', preg_quote( get_home_url() ) ) . '/',
				'',
				$image_src[0]
			);
			if ( ! file_exists( $public_path . $path ) ) {
				$image_src[0] = preg_replace( '/\/$/', '', $live_url ) . $path;
			}
			return $image_src;
		}
	);
}
