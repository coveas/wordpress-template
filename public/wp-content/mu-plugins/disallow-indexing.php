<?php
/**
 * Plugin Name:  Disallow Indexing
 * Plugin URI: https://gitlab.com/coveas/wordpress-template
 * Description:  Disallow indexing on non-production environments
 * Author: Eivin Landa
 * Author URI: https://cove.no
 * License: GPL-3.0 or later
 *
 * @version 1.0.9
 * @package cove/wordpress-template
 */

namespace Cove;

if ( env( 'APP_ENV' ) && env( 'APP_ENV' ) !== 'production' && ! is_admin() ) {
	add_action( 'pre_option_blog_public', '__return_zero' );
	add_filter(
		'robots_txt',
		function() {
			$environment = env( 'APP_ENV' );
			return "# Environment: $environment\nUser-agent: *\nDisallow: /";
		}
	);
}
