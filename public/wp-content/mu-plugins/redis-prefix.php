<?php
/**
 * Plugin Name: Redis prefix
 * Plugin URI: https://gitlab.com/coveas/wordpress-template
 * Description: Automatically configure reids cache key based on app name and environment
 * Author: Eivin Landa
 * Author URI: https://cove.no
 * License: GPL-3.0 or later
 *
 * @version 1.0.9
 * @package cove/wordpress-template
 */

namespace Cove;

if ( defined( 'APP_ENV' ) ) {
	if ( ! defined( 'APP_NAME' ) ) {
		wp_die( 'APP_NAME must be defined in .env' );
	}
	if ( ! defined( 'WP_CACHE_KEY_SALT' ) ) {
		$key = md5( APP_ENV . '_' . APP_NAME );
		$key = substr( $key, -5 );
		define( 'WP_CACHE_KEY_SALT', "{$key}_"  );
	}
}
