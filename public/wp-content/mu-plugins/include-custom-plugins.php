<?php
/**
 * Plugin Name: Include custom plugins
 * Plugin URI: https://gitlab.com/coveas/wordpress-template
 * Description: Loads custom plugins from "public/plugins". They are project specific and not installed via composer.
 * Author: Eivin Landa
 * Author URI: https://cove.no
 * License: GPL-3.0 or later
 *
 * @version 1.0.9
 * @package cove/wordpress-template
 */

namespace Cove;

$dirs = glob( "$public_path/plugins/*", GLOB_ONLYDIR );

/**
 * Remove the public path from plugin urls
 */
add_filter(
	'plugins_url',
	function( $url ) {
		global $public_path;
		$path = str_replace( '\\', '/', $public_path );
		$path = preg_replace( '/^\//', '', $path );
		$url  = str_replace( "/wp-content/plugins/{$path}", '/', $url );
		return $url;
	}
);

/**
 * Loop though custom code and require each dir as a plugin
 */
foreach ( $dirs as $dir ) {
	$file = "$dir/plugin.php";
	if ( ! file_exists( $file ) ) {
		$file = "$dir/" . basename( $dir ) . '.php';
	}
	if ( ! file_exists( $file ) ) {
		continue;
	}
	wp_register_plugin_realpath( $file );
	require_once $file;
}

/**
 * Loco translation support for the custom directories.
 */
add_filter(
	'loco_plugins_data',
	function( $data ) use ( $dirs ) {
		foreach ( $dirs as $dir ) {
			if ( ! is_dir( $dir ) ) {
				continue;
			}
			$name = basename( $dir );
			$data[ "$name/plugin.php" ] = [
				'Name'       => $name,
				'TextDomain' => $name,
				'basedir'    => dirname( $dir ),
			];
		}
		return $data;
	}
);
