<?php
/**
 * Plugin Name: Migration settings
 * Plugin URI: https://gitlab.com/coveas/wordpress-template
 * Description: Disables plugins and change options when copying a database from live to test or local.
 * Author: Eivin Landa
 * Author URI: https://cove.no
 * License: GPL-3.0 or later
 *
 * @version 1.0.12
 * @package cove/wordpress-template
 */

namespace Cove;

if ( env( 'APP_ENV' ) && 'production' !== env( 'APP_ENV' ) ) {
	add_action(
		'init',
		function() {
			if ( ! function_exists( 'get_option' ) ) {
				return;
			}

			$settings = get_option( 'migration_settings' );

			if ( empty( $settings ) ) {
				$settings = [
					'version' => '0.0.0',
				];
			}

			if ( 0 >= version_compare( $settings['version'], '1.0.12' ) ) {
				return;
			}

			require_once ABSPATH . 'wp-admin/includes/plugin.php';

			deactivate_plugins(
				[
					'official-facebook-pixel/facebook-for-wordpress.php',
					'post-smtp/postman-smtp.php',
					'facebook-for-woocommerce/facebook-for-woocommerce.php',
					'synch/emonkey_synch.php',
				]
			);

			// Deactivate plugins.
			deactivate_plugins(
				[
					'mailgun/mailgun.php',
					'pixelyoursite-pro/pixelyoursite-pro.php',
					'woochimp/woochimp.php',
					'woocommerce-google-analytics-pro/woocommerce-google-analytics-pro.php',
				]
			);

			// Algolia.
			$algolia_prefix = get_option( 'algolia_index_name_prefix' );
			$environment    = env( 'APP_ENV' );
			if ( ! preg_match( "/^{$environment}_/", $algolia_prefix ) ) {
				// Environment controlled prefix for the index.
				$algolia_prefix = "{$environment}_{$algolia_prefix}";
				update_option( 'algolia_index_name_prefix', $algolia_prefix );
			}

			// Bring Fraktguiden for WooCommerce.
			$bring_settings = get_option( 'woocommerce_bring_fraktguiden_settings' );
			if ( ! empty( $bring_settings ) ) {
				$bring_settings['test_mode'] = 'yes';
				$bring_settings['debug'] = 'yes';
				update_option( 'woocommerce_bring_fraktguiden_settings', $bring_settings );
			}

			// Klarna Checkout for WooCommerce.
			$kco_settings = get_option( 'woocommerce_kco_settings' );
			if ( ! empty( $kco_settings ) ) {
				$kco_settings['testmode'] = 'yes';
				update_option( 'woocommerce_kco_settings', $kco_settings );
			}

			$settings['version'] = '1.0.12';
			update_option( 'migration_settings', $settings );
		}
	);

	add_action(
		'admin_notices',
		function () {
			$environment = env( 'APP_ENV' );
			printf(
				'<div class="notice notice-error"><p>This site is running in a %s environment and production setting has been deactivated!</p></div>',
				$environment
			);
		}
	);
}
