#!/usr/bin/env php
<?php
/**
 * This file is part of WordPress Template.
 *
 * @package cove/wordpress-template
 */

namespace Cove;

require_once 'public/autoload.php';

printf(
	'-u%s -p%s %s',
	DB_USER,
	DB_PASSWORD,
	DB_NAME
);
